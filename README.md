Contains:

A simple URDF robot model of the MekaMon, 

a suitable RViz preset,

a launch file which sets up a GUI with sliders to feed the simulation in RViz fake joint angles.

Run this test with:

    roslaunch mekamon_urdf display.launch model:='$(find mekamon_urdf)/urdf/mekamon.urdf.xacro'

